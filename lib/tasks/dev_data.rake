namespace :dev_data do
  desc 'Fill database with sample data'
  task create: :environment do
    Rake::Task['db:reset'].invoke

    tags = %w(Diet Végétarien Végétalien Sans_gluten Sans_lait)
    tags_lunch = %w(Apéro Plat Dessert Entrée)
    # USERS
    (0..2).each do |_i|
      user = User.create!(name: Faker::Name.name, email: Faker::Internet.email, password: Faker::Internet.password(8))

      # RECIPES
      recipe = user.recipes.create!(title: Faker::Name.title, description: Faker::Lorem.paragraph, image: 'http://lorempixel.com/400/400/food', all_tags: [tags.shuffle.sample(2), tags_lunch.shuffle.sample].join(', '))
      # INGREDIENTS
      (0..2).each do |_i|
        recipe.ingredients.create!(name: Faker::Commerce.product_name)
      end
      # DIRECTIONS
      (0..2).each do |_i|
        recipe.directions.create!(step: Faker::Hipster.sentence)
      end
    end
    puts '===> DB completed !'
  end
end
