require "rails_helper"

RSpec.describe RecipesController, :type => :controller do
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "loads all of the recipes into @recipes" do
      @recipes = Recipe.all
      get :index
      expect(assigns(:recipes)).to match_array(@recipes.to_ary)
    end

    context 'without params[:tags]' do
      it "populates an array of all recipes" do
        recipe_1 = create(:recipe)
        recipe_2 = create(:recipe)
        get :index
        expect(assigns(:recipes)).to match_array([recipe_1,recipe_2])
      end

      it "renders the :index template" do
        get :index
        expect(response).to render_template :index
      end
    end
    context 'with params[:tags]' do
      it "populates an array of recipes having the tags" do
        vegan_recipe = create(:recipe, all_tags: "vegan")
        get :index,  tags: "vegan"
        expect(assigns(:recipes)).to match_array([vegan_recipe])
      end

      it "renders the :index template if the tag doesn't exist" do
        get :index, tags: "vegan"
        expect(response).to render_template :index
      end
    end
  end

  describe 'GET #show' do
    it "assigns the requested recipe to @recipe" do
      recipe = create(:recipe)
      get :show, id: recipe
      expect(assigns(:recipe)).to eq recipe
    end
    it "renders the :show template" do
      recipe = create(:recipe)
      get :show, id: recipe
      expect(response).to render_template :show
    end
  end

  # describe 'GET #new' do
  #   it "assigns a new Recipe to @recipe" do
  #      get :new
  #      expect(assigns(:recipe)).to be_a_new(Recipe)
  #   end

  #   it "renders the :new template" do
  #     get :new
  #     expect(response).to render_template :new
  #   end
  # end

  # describe 'GET #edit' do
  # it "assigns the requested contact to @recipe" do
  #   recipe = create(:recipe)
  #   get :edit, id: recipe
  #   expect(assigns(:recipe)).to eq recipe
  # end

  # it "renders the :edit template" do
  #   recipe = create(:recipe)
  #   get :edit, id: recipe
  #   expect(response).to render_template "edit"
  # end
  # end
end
