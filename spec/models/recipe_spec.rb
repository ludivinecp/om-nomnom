require 'rails_helper'

describe Recipe do

  it "has a valid factory" do
   expect(create(:recipe)).to be_valid
  end

  it "is invalid without a title" do
    recipe = build(:recipe, title: nil)
    recipe.valid?
    expect(recipe.errors[:title]).to include("can't be blank")
  end

  it "is invalid without a image" do
    recipe = build(:recipe, image: nil)
    recipe.valid?
    expect(recipe.errors[:image]).to include("can't be blank")
  end

  it "is valid with multiple tags" do
    recipe = build(:recipe, all_tags: "vegan , healthy")
    expect(recipe).to be_valid
  end

  describe "recipe is votable" do
    it "is votable" do
      expect(Recipe.votable?).to eq true
    end

    before :each do
      @user = create(:user)
      @recipe = create(:recipe)
    end

    it "is possible to upvote on recipe" do
      @recipe.liked_by @user
      expect(@recipe.get_likes.size).to eq 1
    end

    it "is possible to downvote on recipe" do
      @recipe.downvote_from @user
      expect(@recipe.get_dislikes.size).to eq 1
    end

    it "has the good count of votes (upvote and downvote included)" do
      other_user = create(:user)
      @recipe.liked_by @user
      @recipe.downvote_from other_user
      expect(@recipe.votes_for.size).to eq 2
    end
  end

  it "return a sorted array by dates  " do
    recipe2 = create(:recipe, :created_at => 4.day.ago)
    recipe1 = create(:recipe, :created_at => 1.day.ago)
    expect(Recipe.by_dates) == ["created_at DESC"]
  end

  it "could be class as 'done' by user " do
  end
end

