require 'rails_helper'

describe Tag do
  let(:user) { FactoryGirl.create(:tag) }
  it "has a valid factory" do
  FactoryGirl.create(:tag).should be_valid
  end
  it { should have_many(:recipes) }
  it { should have_many(:taggings) }

end
