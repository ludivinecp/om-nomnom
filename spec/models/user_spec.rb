require 'rails_helper'

describe User do

  it "has a valid factory" do
    expect(create(:user)).to be_valid
  end

  it "is invalid without a name" do
    user = build(:user, name: nil)
    user.valid?
    expect(user.errors[:name]).to include("can't be blank")
  end

  it "is invalid with a duplicate email address" do
    create(:user, email: 'tester@example.com')
    user = build(:user, email: 'tester@example.com')
    user.valid?
    expect(user.errors[:email]).to include("has already been taken")
  end

  it "is invalid with a duplicate name" do
    create(:user, name: 'Joe')
    user = build(:user,  name: 'Joe')
    user.valid?
    expect(user.errors[:name]).to include("has already been taken")
  end

  it "returns a User's capitalize name as a string" do
    user = create(:user, name: 'joe')
    expect(user.name).to eq 'Joe'
  end
end
