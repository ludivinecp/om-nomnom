require 'rails_helper'

describe Ingredient do

  it "has a valid factory" do
    expect(create(:ingredient)).to be_valid
  end

  it { should belong_to(:recipe) }

end

