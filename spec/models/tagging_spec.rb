require 'rails_helper'

describe Tagging do
  it "has a valid factory" do
  FactoryGirl.create(:tagging).should be_valid
  end
  it { should belong_to(:recipe) }
  it { should belong_to(:tag) }

end
