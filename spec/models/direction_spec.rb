require 'rails_helper'

describe Direction do

  it "has a valid factory" do
    expect(create(:direction)).to be_valid
  end

  it { should belong_to(:recipe) }

end

