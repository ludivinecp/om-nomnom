require 'faker'

FactoryGirl.define do
  factory :tag do |f|
    f.name {Faker::Name.name}
  end
end
