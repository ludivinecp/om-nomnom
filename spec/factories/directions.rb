require 'faker'

FactoryGirl.define do
  factory :direction do |f|
    f.step {Faker::Lorem.paragraph}
    association :recipe
  end
end
