require 'faker'

FactoryGirl.define do
  factory :ingredient do |f|
    f.name {Faker::Name.name}
    association :recipe
  end
end
