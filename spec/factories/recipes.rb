require 'faker'

FactoryGirl.define do
  factory :recipe do |f|
    f.title {Faker::Name.title}
    f.description {Faker::Lorem.paragraph}
    f.image {Faker::Placeholdit.image("400x400", 'jpeg', 'ffffff', '000')}
    f.all_tags {Faker::Lorem.word}
    association :user #belongs_to
  end
end
