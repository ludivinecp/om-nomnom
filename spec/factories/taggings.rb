require 'faker'

FactoryGirl.define do
  factory :tagging do |f|
    recipe
    tag
  end
end
