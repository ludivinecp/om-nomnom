class Tag < ActiveRecord::Base
  include CapitalizeConcern
  has_many :taggings
  has_many :recipes, through: :taggings

  def self.counts
    select('name, count(taggings.tag_id) as count').joins(:taggings).group('taggings.tag_id')
  end
end
