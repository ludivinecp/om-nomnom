class User < ActiveRecord::Base
  include CapitalizeConcern
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :recipes
  validates :name, presence:true, uniqueness: true
  acts_as_voter
end
