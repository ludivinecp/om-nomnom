class Recipe < ActiveRecord::Base
  has_many :ingredients
  has_many :directions
  has_many :taggings
  has_many :tags, through: :taggings
  belongs_to :user
  acts_as_votable

  # NESTED INGREDIENTS AND DIRECTIONS
  accepts_nested_attributes_for :ingredients,
                                reject_if: proc { |attributes| attributes['name'].blank? },
                                allow_destroy: true
  accepts_nested_attributes_for :directions,
                                reject_if: proc { |attributes| attributes['step'].blank? },
                                allow_destroy: true

  validates :title, :image, presence: true

  # IMAGE
  has_attached_file :image, styles: { medium: '400x400#' }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  scope :by_dates, -> { order(:created_at).reverse_order }

  def all_tags=(names)
    self.tags = names.split(',').map do |name|
      Tag.where(name: name.strip).first_or_create!
    end
  end

  def all_tags
    tags.map(&:name).join(', ')
  end

  def self.tagged_with(name)
    Tag.find_by_name!(name).recipes
  end
end
