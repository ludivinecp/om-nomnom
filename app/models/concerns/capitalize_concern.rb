module CapitalizeConcern
  extend ActiveSupport::Concern

  included do
    before_create :capitalize_name

    def capitalize_name
      name.capitalize!
    end
  end
end
